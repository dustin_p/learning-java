public class Example {
    
    public static void main(String[] args) {
        // Your amazing code goes here...
        System.out.println("We are making a new Pez Dispenser");  // Prints out the fact we are adding a new Pez
        PezDispenser dispenser = new PezDispenser("Yoda"); // Creates a new PezDispenser named "Yoda"
        System.out.printf("The dispenser character is %s\n", 
                                dispenser.getCharacterName()); // Prints out the new Pez dispensers name
                                
        if (dispenser.isEmpty()) {
            System.out.println("It is currently empty"); // Checks if the dispenser is empty, if so it prints out message
        }
        
        System.out.println("Loading...");
        dispenser.load();
        if (!dispenser.isEmpty()) {
            System.out.println("It's no longer empty"); // Prints that it is no longer empty after checking if it's empty "!dispenser.isEmpty"
        }
        
        while (dispenser.dispense()) {
            System.out.println("Chomp!"); //This will continue to run and dispense until empty
        }
        if (dispenser.isEmpty()) {
            System.out.println("Ate all the Pez!"); //if there is no more pez to dispense, it will show this stating there are no more pez
        }
        
        dispenser.load(4);  // loads 4 more pez, then two more pez
        dispenser.load(2);
        while (dispenser.dispense()) {
            System.out.println("Chomp!"); // eats the pz and continues to run and dispense until empty
        }
        try {
            dispenser.load(400); 
            System.out.println("This will never happen"); 
        } catch (IllegalArgumentException iae) {
            System.out.println("Woah there!");
            System.out.printf("Error was: %s\n", iae.getMessage());
        }
    }
}

/* 32 - try is where we expect an error. This tells the system to continue, we will catch (line 35) the error 
Line 33 adds too many pez, overloads the system
34 - will not print because the exception is caused by 33 so it will never be printed
35 - catches the error on 33, type is IllegalArgumentException (we named it iae) 
36 - prints out "whoa there" to the screen
37 - Prints out the errotr to the screen using our iae and the method getMessage  */