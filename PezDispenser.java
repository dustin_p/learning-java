public class PezDispenser {
	public static final int MAX_PEZ = 12; // final makes it so the number (int) cannot be changed
	private String mCharacterName; // Creates Character name class (string because it's not a number (int) or true/false (boolean)
	private int mPezCount; // Creates private class PezCount (int because it's numbers)
	
	public PezDispenser(String characterName){ //Creates PezDispenser with the charactername string puts initial pez count at 0
		mCharacterName = characterName;
		mPezCount = 0;
	}
	
	public boolean dispense() {
		boolean wasDispensed = false;
		if (!isEmpty()) {
			mPezCount--;  //this subtracts a pez from the count, using mPezCount. The -- will remove one from the current count on one line isntead of mPezCount - 1
			wasDispensed = true;
		}
		return wasDispensed; // Returns to was dispensed if not 0
	}
	
	public boolean isEmpty() { // Checks if pez is at 0, if so it continues instead of looping back
		return mPezCount == 0;
	}
	
	public void load() { //loads new pez to full (MAX_PEZ) now that it's empty - new method will be used line 27 on example.java
		load(MAX_PEZ);
	}
	
	public void load(int pezAmount) { 
		int newAmount = mPezCount + pezAmount; 
		if (newAmount > MAX_PEZ) { 
			throw new IllegalArgumentException("Too many PEZ!!!!"); 
		}
		mPezCount = newAmount;
	}
	
	public String getCharacterName() { 
		return mCharacterName;
	}
}